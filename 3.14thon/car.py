# standard library imports
import sqlite3

# local application imports
from utils import int_input, clear, waiting


class Car :

    def __init__(self, owner_id=None, manufacturer=None, model=None) :
        self.owner_id = owner_id
        self.manufacturer = manufacturer
        self.model = model

    def sql_insert(self, connection) :                      # inserts into database.
        cursor = connection.cursor()                        # checks if object already exists in database.
        cursor.execute(f"""SELECT Manufacturer, Model           
                        from Cars WHERE                         
                        Manufacturer =? 
                        AND Model =?""", (self.manufacturer, self.model))

        if cursor.fetchone() is None :                      # if object doesn't exist, it adds it to database.
            connection.execute(f"""INSERT INTO Cars 
                                (Owner_id,
                                Manufacturer,
                                Model) 
                                VALUES 
                                (?, ?, ?)""", (self.owner_id, self.manufacturer, self.model))

    def add_car(self, connection) :                         # manually add car to database.

        print("1. Add a new car.\n"
              "2. Return to main menu.")

        choice = int_input("Input: ")

        if choice == 1 :                    # asks for user input to use for car object and inserts into database.
            try :
                clear()
                self.owner_id = int_input("Enter id of the owner: ")

                clear()
                self.manufacturer = input("Enter car manufacturer: ")

                clear()
                self.model = input("Enter car model: ")

                Car(self.owner_id, self.manufacturer, self.model).sql_insert(connection)

            except sqlite3.OperationalError :
                clear()
                print("Unable to add data, table cars doesn't exist.")
                waiting()

        elif choice == 2 :
            pass

