# standard library imports
import sqlite3

# local application imports
import menus
from car import Car
from utils import csv_converter


def main() :

    connection = sqlite3.connect("db/3_14thon.db", isolation_level=None)

    person_list = csv_converter("data/persons.csv")
    cars_list = [Car("1", "Volvo", "V70"),
                 Car("1", "Tesla", "Model 3"),
                 Car("2", "Saab", "900"),
                 Car("3", "Ford", "S-MAX"),
                 Car("4", "Lexus", "RX"),
                 Car("5", "Suzuki", "Swift"),
                 Car("3", "Dodge", "Viper")]

    menus.table_maker(connection)
    menus.menu(connection, person_list, cars_list)


if __name__ == "__main__" :
    main()
