# standard library imports
import sqlite3

# third party imports
from tabulate import tabulate

# local application imports
from utils import int_input, clear, waiting


def print_cars(connection) :                        # prints the cars table using tabulate and column descriptions.

    try :
        cursor = connection.cursor()
        cursor.execute("""SELECT * FROM Cars;""")

        if cursor.fetchone() is not None :              # checks if entries exists.
            clear()
            cars = connection.execute("""SELECT * FROM Cars;""")
            description = tuple(map(lambda x : x[0], cars.description))
            print(tabulate(cars, description))
            waiting()

        else :
            clear()
            print("You haven't loaded any data yet.")
            waiting()

    except sqlite3.OperationalError :
        clear()
        print("You haven't created any tables yet.")
        waiting()


def print_join(connection) :                # prints the person/cars joined output using tabulate.

    try :
        clear()
        cursor = connection.cursor()
        cursor.execute("""SELECT * FROM Cars;""")

        if cursor.fetchone() is not None :          # checks if entries exists.
            person_cars = connection.execute("""SELECT Owner_id, Person.Firstname,
                                                Person.Lastname, Cars.Manufacturer, Cars.Model FROM Cars
                                                INNER JOIN Person on Cars.Owner_id=Person.id;""")

            description = tuple(map(lambda x : x[0], person_cars.description))
            print(tabulate(person_cars.fetchall(), description))
            waiting()

        else :
            clear()
            print("You haven't loaded any data yet.")
            waiting()

    except sqlite3.OperationalError :
        clear()
        print("You haven't created any tables yet.")
        waiting()


def print_persons(connection) :                 # prints the person table using tabulate and column descriptions.

    try :
        cursor = connection.cursor()
        cursor.execute("""SELECT * FROM Person;""")

        if cursor.fetchone() is not None :  # checks if entries exists.
            clear()
            persons = connection.execute("""SELECT * FROM Person;""")
            description = tuple(map(lambda x : x[0], persons.description))
            print(tabulate(persons, description))
            waiting()

        else :
            clear()
            print("You haven't loaded any data yet.")
            waiting()

    except sqlite3.OperationalError :
        clear()
        print("You haven't created any tables yet.")
        waiting()


def query_firstname(connection) :                   # queries database with lastname and retrieves entry.

    try :
        clear()
        cursor = connection.cursor()
        firstname = input("Enter firstname: ")
        cursor.execute(f"""SELECT * FROM Person WHERE Firstname="{firstname.capitalize()}";""")
        print_column = connection.execute(f"""SELECT * FROM Person""")
        description = tuple(map(lambda x : x[0], print_column.description))

        clear()
        print(tabulate(cursor.fetchall(), description))
        waiting()

    except sqlite3.OperationalError :
        clear()
        print("No data.")
        waiting()


def query_lastname(connection) :            # queries database with lastname and retrieves entry.

    try :
        clear()
        cursor = connection.cursor()
        lastname = input("Enter lastname: ")
        cursor.execute(f"""SELECT * FROM Person WHERE Lastname="{lastname.capitalize()}";""")
        print_column = connection.execute(f"""SELECT * FROM Person""")
        description = tuple(map(lambda x : x[0], print_column.description))

        clear()
        print(tabulate(cursor.fetchall(), description))
        waiting()

    except sqlite3.OperationalError :
        clear()
        print("No data.")
        waiting()


def query_birthdate(connection) :           # queries database with birthdate and retrieves entry.

    try :
        clear()
        cursor = connection.cursor()
        birthdate = input("Enter birthdate: ")
        cursor.execute(f"""SELECT * FROM Person WHERE Birthdate="{birthdate.capitalize()}";""")
        print_column = connection.execute(f"""SELECT * FROM Person""")
        description = tuple(map(lambda x : x[0], print_column.description))

        clear()
        print(tabulate(cursor.fetchall(), description))
        waiting()

    except sqlite3.OperationalError :
        clear()
        print("No data.")
        waiting()


def query_address(connection) :             # queries database with address and retrieves entry.

    try :
        clear()
        cursor = connection.cursor()
        address = input("Enter address: ")
        cursor.execute(f"""SELECT * FROM Person WHERE Address="{address.capitalize()}";""")
        print_column = connection.execute(f"""SELECT * FROM Person""")
        description = tuple(map(lambda x : x[0], print_column.description))

        clear()
        print(tabulate(cursor.fetchall(), description))
        waiting()

    except sqlite3.OperationalError :
        clear()
        print("No data.")
        waiting()


def delete_entry(connection) :              # deletes entry of chosen id.

    try :
        clear()
        cursor = connection.cursor()
        table_id = int_input("Enter id to delete: ")
        cursor.execute(f"""SELECT id FROM Person WHERE id="{table_id}";""")

        if cursor.fetchone() is None :          # checks if chosen id exists
            print("No data to delete.")
            waiting()

        else :                                  # if id exists, it gets deleted.
            connection.execute(f"""DELETE FROM Person WHERE id="{table_id}";""")

    except sqlite3.OperationalError :
        clear()
        print("No data.")
        waiting()


def update_address(connection) :            # updates address of chosen id.

    try :
        clear()
        cursor = connection.cursor()
        table_id = int_input("Enter id to update: ")
        cursor.execute(f"""SELECT id FROM Person WHERE id ="{table_id}";""")

        if cursor.fetchone() is None :          # checks if chosen id exists.
            print("No data to update.")
            waiting()

        else :
            clear()                              # asks for replacement address and updates it.
            address_change = input("Enter new address: ")
            connection.execute(f"""UPDATE Person SET address=? WHERE id=?;""", (address_change, table_id))

    except sqlite3.OperationalError :
        clear()
        print("No data.")
        waiting()


def create_default(connection) :                # creates default database tables person and cars.

    connection.execute(f"""CREATE TABLE IF NOT EXISTS Person (
                            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                            Firstname,
                            Lastname,
                            Birthdate,
                            Address);""")

    connection.execute(f"""CREATE TABLE IF NOT EXISTS Cars (
                            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                            Owner_id,
                            Manufacturer,
                            Model);""")

    print(f"Created default table for Persons and Cars.")


def define_table(connection) :                  # defines a table and columns from user input.

    table_name = ""
    while True :
        try :
            table_name = input("Enter table name: ")
            connection.execute(
                f"""CREATE TABLE {table_name} (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL);""")

        except sqlite3.OperationalError :
            clear()
            print("Enter a string without numbers.")  # catches exception if numbers are entered.
            continue

        else :
            break

    clear()
    column_output = ""
    counter = 1
    additional = int_input("Enter number of columns: ")

    while additional > 0 :                         # loops until chosen number of columns have been created.
        clear()
        next_column = ""
        while True :
            try:
                next_column = input(f"Choose name of the {counter} column: ")
                connection.execute(f"""ALTER TABLE {table_name} ADD COLUMN {next_column}""")

            except sqlite3.OperationalError :
                clear()
                print("Enter a string without numbers.")
                continue

            else :
                break

        column_output += f"{next_column} "
        counter += 1
        additional -= 1

    clear()
    print(f"Table: {table_name} Columns: {column_output}")


def insert_data(connection, person_list, cars_list) :

    try :
        for person in person_list :
            person.sql_insert(connection)

        for car in cars_list :
            car.sql_insert(connection)

    except sqlite3.OperationalError :
        clear()
        print("You haven't created any tables yet.")
        waiting()
