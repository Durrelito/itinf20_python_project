# standard library imports
import csv
import os

# local application imports
from person import Person


def int_input(question) :                       # handles int input exceptions.

    while True :
        try :
            return int(input(question))

        except ValueError :
            clear()
            print("Please enter a valid integer.")


def waiting() :                                 # waits for user input
    input(f"\nPress Enter to continue.")


def clear() :
    if os.name == 'posix' :                        # clears terminal for Mac / Linux users
        _ = os.system('clear')

    else :                                      # clears terminal for Windows users
        _ = os.system('cls')


def csv_converter(path) :                       # adds rows from a csv file to a new list.
    with open(path) as c :
        read_csv = csv.reader(c)
        converted_list = []
        for row in read_csv :
            converted_list.append(Person(*row))

        return converted_list[1:]
